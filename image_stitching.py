from os import path
import cv2
import numpy as np
from matplotlib import pyplot as plt

data_path = "../data/"
left_image_name = "left.jpeg"
right_image_name = "right.jpeg"


def check_path(file_path):
    if not path.exists(file_path):
        print("No such file!")
        exit()


def detect_and_compute(img):
    # Initiate STAR detector
    orb = cv2.ORB_create()

    # find the keypoints with ORB
    kp = orb.detect(img, None)

    # compute the descriptors with ORB
    kp, des = orb.compute(img, kp)

    return kp, des


def main():
    # Left and Right images
    path_to_left = data_path + left_image_name
    path_to_right = data_path + right_image_name
    check_path(path_to_left)
    check_path(path_to_right)

    # a. load + convert
    left_image = cv2.imread(path_to_left)  # queryImage
    right_image = cv2.imread(path_to_right)  # trainImage

    left_image_gray = cv2.cvtColor(left_image, cv2.COLOR_RGB2GRAY)
    right_image_gray = cv2.cvtColor(right_image, cv2.COLOR_RGB2GRAY)

    # c. keypoints and descriptors for both images
    kp_left, des_left = detect_and_compute(left_image_gray)
    kp_right, des_right = detect_and_compute(right_image_gray)

    # d. Compute distances between descriptors
    # BFMatcher with default params
    bf = cv2.BFMatcher()
    matches = bf.knnMatch(des_right, des_left, k=2)

    # e. Apply ratio test
    good_matches = [m for m, n in matches if m.distance < 0.7 * n.distance]

    # f. Compute Homography
    dst = np.float32([kp_left[m.trainIdx].pt for m in good_matches]).reshape(-1, 1, 2)
    src = np.float32([kp_right[m.queryIdx].pt for m in good_matches]).reshape(-1, 1, 2)
    H, mask = cv2.findHomography(src, dst, cv2.RANSAC, 5.0)

    # g. Warp one of the images
    height_l, width_l, _ = left_image.shape
    height_r, width_r, _ = right_image.shape
    out_img = cv2.warpPerspective(right_image, H, (width_l+width_r, height_l))

    # h. Stitch the two images together and plot the result using Matplotlib
    out_img[: height_l, : width_l, :] = left_image
    out_img_rgb = cv2.cvtColor(out_img, cv2.COLOR_BGR2RGB)
    plt.imshow(out_img_rgb)
    plt.show()


if __name__ == "__main__":
    main()
