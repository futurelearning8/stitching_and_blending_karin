import numpy as np
import scipy as sp
import scipy.signal
from scipy.signal import convolve2d
import cv2
from os import path
from matplotlib import pyplot as plt
import math


def generatingKernel(parameter):
    """ Return a 5x5 generating kernel based on an input parameter.
    Note: This function is provided for you, do not change it.
    Args:
      parameter (float): Range of value: [0, 1].
    Returns:
      numpy.ndarray: A 5x5 kernel.
    """
    kernel = np.array([0.25 - parameter / 2.0, 0.25, parameter,
                       0.25, 0.25 - parameter / 2.0])
    return np.outer(kernel, kernel)


def reduce(image):
    """ Convolve the input image with a generating kernel of parameter of 0.4 and
    then reduce its width and height by two.
    Args:
      image (numpy.ndarray): a grayscale image of shape (r, c)
    Returns:
      output (numpy.ndarray): an image of shape (ceil(r/2), ceil(c/2))
        For instance, if the input is 5x7, the output will be 3x4.
    """
    # for Convolution
    kernel = generatingKernel(0.4)
    # Convolve this kernel on the image with the ‘same’ padding.
    image = convolve2d(image, kernel, mode="same")

    # subsamples it down to a quarter of the size
    image = image[::2, ::2]

    return image


def expand(image):
    """ Expand the image to double the size and then convolve it with a generating
    kernel with a parameter of 0.4.
    You should upsample the image, and then convolve it with a generating kernel
    of a = 0.4.
    Finally, multiply your output image by a factor of 4 in order to scale it
    back up. If you do not do this (and I recommend you try it out without that)
    you will see that your images darken as you apply the convolution (why?). 
    Args:
      image (numpy.ndarray): a grayscale image of shape (r, c)
    Returns:
      output (numpy.ndarray): an image of shape (2*r, 2*c)
    """
    image_shape = image.shape
    unsampled_img_shape = (image_shape[0] * 2, image_shape[1] * 2)

    unsampled_image = np.zeros((unsampled_img_shape))
    unsampled_image[::2, ::2] = image

    # for Convolution
    kernel = generatingKernel(0.4)
    # Convolve this kernel on the image with the ‘same’ padding.
    convolved_expanded_img = convolve2d(unsampled_image, kernel, mode="same")
    expanded = convolved_expanded_img * 4

    return expanded


def gaussPyramid(image, levels):
    """ Construct a pyramid from the image by reducing it by the number of levels
    passed in by the input.
    Note: You need to use your reduce function in this function to generate the
    output.
    Args:
      image (numpy.ndarray): A grayscale image of dimension (r,c) and dtype float.
      levels (uint8): A positive integer that specifies the number of reductions
                      you should do. So, if levels = 0, you should return a list
                      containing just the input image. If levels = 1, you should
                      do one reduction. len(output) = levels + 1
    Returns:
      output (list): A list of arrays of dtype np.float. The first element of the
                     list (output[0]) is layer 0 of the pyramid (the image
                     itself). output[1] is layer 1 of the pyramid (image reduced
                     once), etc. We have already included the original image in
                     the output array for you. The arrays are of type
                     numpy.ndarray.
    """
    output = [image]
    reduced_image = reduce(image)
    while levels:
        output.append(reduced_image)
        reduced_image = reduce(reduced_image)
        levels = levels - 1

    return output


def laplPyramid(gaussPyr):
    """ Construct a laplacian pyramid from the gaussian pyramid, of height levels.
    Note: You must use your expand function in this function to generate the
    output. The Gaussian Pyramid that is passed in is the output of your
    gaussPyramid function.
    Args:
      gaussPyr (list): A Gaussian Pyramid as returned by your gaussPyramid
                       function. It is a list of numpy.ndarray items.
    Returns:
      output (list): A laplacian pyramid of the same size as gaussPyr. This
                     pyramid should be represented in the same way as guassPyr,
                     as a list of arrays. Every element of the list now
                     corresponds to a layer of the laplacian pyramid, containing
                     the difference between two layers of the gaussian pyramid.
             output[k] = gauss_pyr[k] - expand(gauss_pyr[k + 1])
             Note: The last element of output should be identical to the last
             layer of the input pyramid since it cannot be subtracted anymore.
    Note: Sometimes the size of the expanded image will be larger than the given
    layer. You should crop the expanded image to match in shape with the given
    layer.
    For example, if my layer is of size 5x7, reducing and expanding will result
    in an image of size 6x8. In this case, crop the expanded layer to 5x7 by
    removing the last row/column.
    """
    laplPyr = []

    gaussPyr = gaussPyr.copy()
    gaus_var = gaussPyr.pop()
    laplPyr.append(gaus_var)
    while gaussPyr:
        expanded_gaus_var = expand(gaus_var)
        gaus_var = gaussPyr.pop()
        # fix shape
        expanded_gaus_var = expanded_gaus_var[:gaus_var.shape[0], :gaus_var.shape[1]]
        diff = cv2.subtract(gaus_var, expanded_gaus_var)
        laplPyr.append(diff)

    # reverse order
    laplPyr.reverse()
    return laplPyr


def blend(laplPyrWhite, laplPyrBlack, gaussPyrMask):
    """ Blend the two laplacian pyramids by weighting them according to the
    gaussian mask.
    Args:
      laplPyrWhite (list): A laplacian pyramid of one image, as constructed by
                           your laplPyramid function.
      laplPyrBlack (list): A laplacian pyramid of another image, as constructed by
                           your laplPyramid function.
      gaussPyrMask (list): A gaussian pyramid of the mask. Each value is in the
                           range of [0, 1].
    The pyramids will have the same number of levels. Furthermore, each layer
    is guaranteed to have the same shape as previous levels.
    You should return a laplacian pyramid that is of the same dimensions as the
    input pyramids. Every layer should be an alpha blend of the corresponding
    layers of the input pyramids, weighted by the gaussian mask. This means the
    following computation for each layer of the pyramid:
      output[i, j] = current_mask[i, j] * white_image[i, j] +
                     (1 - current_mask[i, j]) * black_image[i, j]
    Therefore:
      Pixels where current_mask == 1 should be taken completely from the white
      image.
      Pixels where current_mask == 0 should be taken completely from the black
      image.
    Note: current_mask, white_image, and black_image are variables that refer to
    the image in the current layer we are looking at. You do this computation for
    every layer of the pyramid.
    """
    blended_pyr = []
    for white_lapl_val, black_lapl_val, gauss_mask_val in zip(laplPyrWhite, laplPyrBlack, gaussPyrMask):
        blended_pyr.append(gauss_mask_val * white_lapl_val + (1 - gauss_mask_val) * black_lapl_val)

    return blended_pyr


def collapse(pyramid):
    """ Collapse an input pyramid.
    Args:
      pyramid (list): A list of numpy.ndarray images. You can assume the input is
                    taken from blend() or laplPyramid().
    Returns:
      output(numpy.ndarray): An image of the same shape as the base layer of the
                             pyramid and dtype float.
    Approach this problem as follows, start at the smallest layer of the pyramid.
    Expand the smallest layer, and add it to the second to smallest layer. Then,
    expand the second to smallest layer, and continue the process until you are
    at the largest image. This is your result.
    Note: sometimes expand will return an image that is larger than the next
    layer. In this case, you should crop the expanded image down to the size of
    the next layer. Look into numpy slicing / read our README to do this easily.
    For example, expanding a layer of size 3x4 will result in an image of size
    6x8. If the next layer is of size 5x7, crop the expanded image to size 5x7
    by removing the last row/column.
    """

    expanded_pyr_val = expand(pyramid[-1])
    for pyr_val in reversed(pyramid[:-1]):
        expanded_gaus_var = expanded_pyr_val[:pyr_val.shape[0], :pyr_val.shape[1]]
        expanded_pyr_val = pyr_val + expanded_gaus_var
        expanded_pyr_val = expand(expanded_pyr_val)

    return expanded_pyr_val


def check_path(file_path):
    if not path.exists(file_path):
        print("No such file!")
        exit()


def main():
    data_path = '../data/'
    # Load images
    black_image_path = data_path + "black.jpg"
    black_image = cv2.imread(black_image_path).astype(float)

    white_image_path = data_path + "white.jpg"
    white_image = cv2.imread(white_image_path).astype(float)

    mask_image_path = data_path + "mask.jpg"
    mask_image = cv2.imread(mask_image_path).astype(float) / 255

    # find depth accordind smallest shape
    min_size = min(black_image.shape[0], black_image.shape[1], white_image.shape[0], white_image.shape[1])
    depth = int(math.floor(math.log(min_size, 2)))

    blended_images = []
    # run for R, G, B
    for channel in range(3):
        # certain channel
        black_image_gray_c = black_image[:, :, channel]
        white_image_gray_c = white_image[:, :, channel]
        mask_image_gray_c = mask_image[:, :, channel]

        # bulid all required pyramids
        black_gaussPyramid_images = gaussPyramid(black_image_gray_c, depth)
        black_laplPyramid_images = laplPyramid(black_gaussPyramid_images)
        white_gaussPyramid_images = gaussPyramid(white_image_gray_c, depth)
        white_laplPyramid_images = laplPyramid(white_gaussPyramid_images)
        mask_gaussPyramid_images = gaussPyramid(mask_image_gray_c, depth)

        # blend strp + collapse
        blended_pyramids_c = blend(white_laplPyramid_images, black_laplPyramid_images, mask_gaussPyramid_images)
        blended_images_c = collapse(blended_pyramids_c)
        # append channel result
        blended_images.append(blended_images_c)

    # merge all channels and display
    merged = cv2.merge(blended_images)
    merged[merged < 0] = 0
    merged[merged > 255] = 255
    # for converting color
    merged = merged.astype("float32")
    merged = cv2.cvtColor(merged, cv2.COLOR_BGR2RGB)
    # for displaying
    merged = merged.astype(np.uint8)
    plt.imshow(merged)
    plt.show()


if __name__ == "__main__":
    main()
